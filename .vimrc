set nocompatible              " be iMproved, required
filetype off                  " required
set hidden
set laststatus=2 " required for lightline
set noshowmode " also required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'itchyny/lightline.vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
"filetype plugin on " required

" Important things

" Colors
syntax enable " enables syntax
set background=dark

let base16colorspace=256  " Access colors present in 256 colorspace

" Custom colors
"colorscheme custom_one
colorscheme gruvbox
"colorscheme hopscotch

" extrem way if disabling black border
hi Normal ctermbg=none

" Useful additions
set tabstop=2 " makes tab 2 chars instead of 8
set softtabstop=2 " 
set number " set numbered line
set cursorline " shows cursor line
set wildmenu " autocomplete (?)

" Mapping to some useful keys
map <F2> :!ls<CR>:Explore

" Backup settings
set backup " enables backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp " directory of backup
set backupskip=/tmp/*,/private/tmp
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup " enables write backup (eg. on save)
