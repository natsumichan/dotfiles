# Created by newuser for 5.4.1

#eval 'dircolors ~/.dir_colors'

#fpath=( "$HOME/.zsh_functions" $fpath )

autoload -U promptinit; promptinit
#prompt pure

# include z.sh
. $HOME/.config/scripts/z.sh

#BASE16_SHELL=$HOME/.config/base16-shell/
#[ -n "$PS1" ] && [ -s $BASE16_SHELL/profile_helper.sh ] && eval "$($BASE16_SHELL/profile_helper.sh)"

source ~/.aliases
setopt PROMPT_SUBST

# custom prompt
PROMPT='[%n@%m %~]$ '
#RPROMPT='[]'

function prompt_char () {
	git branch >/dev/null 2>/dev/null && echo '±' && return
	echo '➔'
}

function prompt_builder() {
echo '%F{cyan}%n%f at %F{yellow}%m%f in %F{green}%~%f
$(prompt_char) '
}

LS_COLORS=$LS_COLORS:'di=0;30;107:ex=0;30;92' ; export LS_COLORS
